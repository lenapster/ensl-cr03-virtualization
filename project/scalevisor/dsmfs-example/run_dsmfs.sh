#!/bin/bash

#module should already be inserted (dsmfs.ko)

set -x

DEFAULTPROCNB=2
NPROC=${1:-$DEFAULTPROCNB}
dpath="./dsmfiles/$(date +%s)"

#kills all processes at exit 
trap 'kill $(jobs -p) || true' EXIT

#create all dirs /media/node0; /media/node1; ...
for i in $(seq 0 $((NPROC-1)))
do
	mkdir -p "$dpath"$i
done

#mount file systems /media/node0; /media/node1; ...
for i in $(seq 0 $((NPROC-1)))
do
	sudo mount -t dsmfs -osize=6G,mode=1777,id=$i  tmpfs "$dpath"$i
done

#run applications using diffrent paths ...
for i in $(seq 0 $((NPROC-1)))
do
	./counter_comm.exe $i $NPROC "$dpath"$i/file.txt &
	#needed to usure that the files are created in order
	sleep 3
done

#wait for all processes to finish
wait
