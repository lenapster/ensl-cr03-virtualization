#include <errno.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <limits.h>

extern int errno;

#define MAXVALUE INT_MAX

/* nproc==number of process
 * procid== process id 
 * example:
 * 2 process: nproc == 2
 * process 0: id is 0
 * process 1: id is 1
 */
void file_access(void* mymap, int nproc, int procid, int maxvalue)
{
	volatile int* counter = (int*) mymap;

	while(*counter<maxvalue)
	{
		while((*counter % nproc)!=procid)
			sleep(1);
		printf("node %d %d\n", procid, *counter);
		fflush(stdout);
		(*counter)++;
	}

}

int munmap_file(void* mymap, size_t size)
{
	if (munmap(0, size) == -1) {
		fprintf(stderr, "%s: Error munmap\n",strerror(errno));
		exit(-1);
	}
}

void* mmap_file(char* filePath, size_t size)
{
	int fd;
	void *mymap;

	fd = open(filePath, O_RDWR| O_CREAT, S_IRWXU);
	if (fd == -1) {
		perror("Error opening file");
		exit(1);
	}
	if(ftruncate(fd, size)) {
		fprintf(stderr,"Error ftruncate\n");
		close(fd);
		exit(1);
	}

	mymap = mmap(0, size, PROT_READ|PROT_WRITE, MAP_FILE|MAP_SHARED, fd, 0);

	if(mymap == MAP_FAILED) {
		fprintf(stderr, "%s: Fehler bei mmap\n",strerror(errno));
		close(fd);
		exit(1);
	}

	return mymap;
}



int main(int argc, char *argv[])
{
	int id;
	int num;
	int fd;
	char *path;
	void *mymap;
	int file_size;
	
	if(argc<3)
	{
		printf("usage: ./%s id num path", argv[0]);
		return -1;
	}

	id=atoi(argv[1]);
	num=atoi(argv[2]);
	path=argv[3];

	file_size=num*8;
	mymap=mmap_file(path, file_size);

	file_access(mymap, num, id, MAXVALUE);
	munmap_file(mymap, file_size);

	return 0;
}
