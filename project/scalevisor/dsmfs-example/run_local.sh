#!/bin/bash

#run using a single file (using file system is used in /tmp).
#to modify the file path chek the dpath variable

#This allows to check for expeted behavior that will
#be emulated by DSMFS

set -x

DEFAULTVALUE=4
NPROC=${1:-$DEFAULTVALUE}
dpath="/tmp/test_simple"

#kills all processes at exit 
trap 'kill $(jobs -p) || true' EXIT

#create all dirs
mkdir -p "$dpath"

#run applications
for i in $(seq 0 $((NPROC-1)))
do
	./counter_comm.exe $i $NPROC "$dpath"/file.txt &
done

#wait for all processes to finish
wait
