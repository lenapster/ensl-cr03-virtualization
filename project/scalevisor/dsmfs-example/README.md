# What it does ?
It creates two processes that will communicate through dsmfs files. The files will be on 
different mounted file systems. The code of the process can be found in "counter_comm.c".
The two process will increment a shared counter "mmaped" in the dsmfs files. Process A
will increment the counter when it has an even value. Process B will increment the counter
when it has an odd value. This icrementation loop continue until reaching MAX_VALUE (fill
free to modify it) or the program is explicitly stopped through a signal (Ctrl^C).

# How to run the example ?
To run the example you must first have loaded the dsmfs kernel module. Once loaded you
need to compile the code in "counter_comm.c". You can use the "make" command for this.
After that you can use the "run_dsmfs.sh" to run the two processes: 
$ ./run_dsmfs.sh 2

Note: the script and the program can run more than two processes that will communicate
through the shared counter !

# Testing without DSMFS
To test the example using a local file system refer to "run_local.sh".
