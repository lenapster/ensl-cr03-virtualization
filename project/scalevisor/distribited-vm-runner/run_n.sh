#!/bin/bash

set -xe

NPROC=${1:-4}
IMAGE=debian86.qcow2
QEMU=../qemu-giantvm/x86_64-softmmu/qemu-system-x86_64 #FIXME
KERNEL=bzImage

#TODO: use a single path for all runs
#for now this is a workarround
MEMORY_FILE_PATH=/media/$(date +%s)
INITRD=initramfs.cpio.gz
NODE_IP="127.0.0.1"
MEMORY_SIZE=1024

#sudo truncate -s0 /var/log/kern.log

function get_iplist
{
	NB=$1
	IPLIST=""
	for i in $(seq $NB)
        do
                IPLIST+="$NODE_IP "
        done
	echo $IPLIST
}

function mount_one
{
	ID=$1
	MEMORY_PATH="$2"$ID
	sudo mkdir -p $MEMORY_PATH
	sudo mount -t dsmfs -osize=6G,mode=1777,id=$ID  tmpfs $MEMORY_PATH
	sudo chown toto:toto $MEMORY_PATH
}

function run_one
{
	ID=$1
	NB=$2
	MEMORY_FILE="$MEMORY_FILE_PATH"$ID/ram.img
	IPLIST="$(get_iplist $NB)"
	TELNET_PORT=$((ID+1234))

	$QEMU -nographic -kernel $KERNEL \
	-initrd $INITRD \
	-cpu host -machine kernel-irqchip=off \
	-hda $IMAGE \
	-shm-path $MEMORY_FILE \
	-append "root=/dev/sda console=ttyS0 " \
	-smp $NB -m $MEMORY_SIZE -enable-kvm -serial mon:stdio \
	-local-cpu 1,start=$ID,iplist="$IPLIST" \
	-monitor telnet:$NODE_IP:$TELNET_PORT,server,nowait 
}


#mount all
for i in $(seq 0 $((NPROC-1)))
do
	echo "mount_one $i $MEMORY_FILE_PATH"
	mount_one $i $MEMORY_FILE_PATH
done

sleep 2

#run all
for i in $(seq 0 $((NPROC-1)))
do
	echo "run_one $i $NPROC &"
	run_one $i $NPROC &
	sleep 5
done

wait
#pkill -9 "qemu-system"
