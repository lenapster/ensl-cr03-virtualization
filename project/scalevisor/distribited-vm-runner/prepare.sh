#!/bin/bash

set -e

################ SOME GLOBAL VARIABLES ##########################
#project folder
export OPT=$(pwd)
#build files/folders
export BUILDS=$OPT/builds
#debian varaibles
export DEBIAN=$BUILDS/debian
#ramfs dir
export INIT_RAMFS=$OPT/initramfs.cpio.gz
#hard drive
export HARD_DRIVE=$OPT/debian86.qcow2


mkdir -p $BUILDS


### Get distribution files
if [ ! -d $DEBIAN ];then
	git clone -b noboot  https://github.com/ScaleVisor/debian-distrib.git $DEBIAN
else
	#update
	cd $DEBIAN
	git pull
	cd -
fi

### Create initramfs
cd $DEBIAN
#add init file
cp $OPT/init $DEBIAN/init; chmod +x $DEBIAN/init
find .  ! -path  "*.git*" -print0 | cpio --null -ov --format=newc | gzip -9 >  $INIT_RAMFS
cd -

### Creat hard drive: for now everything is in initramfs (the disk is empty)
touch $HARD_DRIVE

### create bzimage
cd $OPT
ln -s -f bzImage-page-align bzImage
