### Assumptions
The current installed kernel is  linux 4.9 with the dsmfs module installed.

### Prepare the simulation environment
$ ./prepare.sh

This command will build an initaramfs and the hard drive (empty for now)
TODO: Ideally we will also want to build the guest kernel. [instead of the bzimages]
TODO: Also build qemu

### Run  the distribted VM

./run_n.sh X

Where X is number of sub-VMs. The sub-VMs will communicate through dsmfs to give
the illusion of a single VM. 

Please set the right variables in the top of the file. The most important being QEMU.
For now we have tested with qemu found at:
https://github.com/moharaka/qemu-giantvm.git (branch dsmfs)

