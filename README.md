This projet includes all the documents related to the virtualization class (CR03-Virtualization technologies: Design and Implementation) that I am responsible in ENS Lyon, France.
For any comments, please contact me alain[dot]tchana[at]ens-lyon[dot]fr.

Thank you to Stella Bitchebe, Bao Bui, Mohamed Karaoui and job Mvondo, Ph.D. students and post-doc in my group, for their help.

The slides are in "courses"
The practical classes are in "practical_classes"
Research papers in "papers"
Books are in "books"
The presentation of the project in "project"
